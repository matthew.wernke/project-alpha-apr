from django.contrib import admin
from projects.models import Project


class ProjectAdmin(Project):
    pass


admin.site.register(Project)
