from django.contrib import admin
from tasks.models import Task


class TaskAdmin(Task):
    pass


admin.site.register(Task)
